import Vue from 'vue'
import App from './App'
//数据管理中心
import store from '@/store'
Vue.prototype.$store = store;
// 工具
import '@/common/utils.js';
//权限配置中心
import base from '@/config/baseUrl'
Vue.prototype.$base = base;
//挂载全局http请求
import $http from '@/config/requestConfig'
Vue.prototype.$http = $http;
// #ifdef MP-WEIXIN
//挂载全局微信分享
import { wxShare } from '@/config/utils'
Vue.prototype.wxShare = wxShare;
// #endif

//判断是否登录
import { judgeLogin } from '@/config/login';
Vue.prototype.judgeLogin = judgeLogin;
Vue.config.productionTip = false;

import Vant from 'vant';
// import 'vant/lib/index.css';
Vue.use(Vant);
import VueTouch from 'vue-touch'
Vue.use(VueTouch, {name: 'v-touch'})

//页面跳转(不判断登录状态)
Vue.prototype.onPageJump =function onPageJump(url) {
	uni.navigateTo({
		url: url
	});
};
//页面跳转(判断登录状态)
Vue.prototype.onTokenJump =function onTokenJump(url) {
	this.judgeLogin(() => {
		uni.navigateTo({
			url: url
		});
	});
};
// #ifdef H5
//微信SDK
import '@/common/wxJsSDK.js';
// #endif
//全局组件
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue";
Vue.component("mescroll-body", MescrollBody);
App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount();