import $http from '@/config/requestConfig'

//获取用户练习和考试统计
export const getUserStatistics = (param) => {
    return $http.get("qiuxue/app/userSubjectNow/getUserStatistics", param);
};

//存储当前科目ID
export const userSubjectNowSave = (param) => {
    return $http.post("qiuxue/app/userSubjectNow/save", param);
};

// 获取科目树
export const subjectTypeTree = (param) => {
    return $http.get("qiuxue/app/subjectType/subjectTypeTree", param);
};

//获取上次保存的科目位置
export const userSubjectNowGet = (param) => {
    return $http.get("qiuxue/app/userSubjectNow/get", param);
};

// 获取已购买的答题卡
export const queryAnswerSheetList = (param) => {
    return $http.post("qiuxue/app/answerSheet/queryAnswerSheetList", param);
};


// 顺序刷题
export const getQuestionIds = (param) => {
    return $http.put("qiuxue/app/question/getQuestionIds", param);
};
export const batchListById = (param) => {
    return $http.put("qiuxue/app/question/batchListById", param);
};
export const cancelCollectQuestion = (questionId) => {
    return $http.get("qiuxue/app/collect/cancelCollectQuestion/"+ questionId);
};
export const collectQuestion = (param) => {
    return $http.post("qiuxue/app/collect/collectQuestion",param);
};
export const queryAnswerStats = (param) => {
    return $http.post("qiuxue/app/question/queryAnswerStats",param);
};
export const answerReset = (param) => {
    return $http.post("qiuxue/app/answerRecord/answerReset",param);
};
export const saveAnswerRecord = (param) => {
    return $http.post("qiuxue/app/answerRecord/saveAnswerRecord",param);
};

// 随机刷题
export const getRandomQuestionIds = (param) => {
    return $http.put("qiuxue/app/question/getRandomQuestionIds", param);
};
export const randomList = (param) => {
    return $http.post("qiuxue/app/question/randomList", param);
};



//我的答题卡
export const queryAnswerSheet = (param) => {
    return $http.post("qiuxue/app/answerSheet/queryAnswerSheet", param);
};
export const answerSheetList = (param) => {
    return $http.get("qiuxue/app/answerSheet/list", param);
};
export const redeemRecordAdd = (param) => {
    return $http.post("qiuxue/app/redeemRecord/add", param);
};


export const outlineList = (param) => {
    return $http.get("qiuxue/app/outline/list", param);
};
export const sequenceList = (param) => {
    return $http.post("qiuxue/app/question/sequenceList", param);
};
export const getRecordList = (param) => {
    return $http.get("qiuxue/app/testPaper/getRecordList/", param);
};
export const delTestRecord = (param) => {
    return $http.post("qqiuxue/app/testPaper/delTestRecord", param);
};
export const getRecordDetails = (param) => {
    return $http.get("qiuxue/app/testPaper/getRecordDetails", param);
};
export const queryCollectQuestionList = (param) => {
    return $http.post("qiuxue/app/collect/queryCollectQuestionList", param);
};
export const queryWrongAnswerQuestion = (param) => {
    return $http.post("qiuxue/app/question/queryWrongAnswerQuestion", param);
};
export const testPaperList = (param) => {
    return $http.get("qiuxue/app/testPaper/list", param);
};
export const paperDetail = (param) => {
    return $http.get("qiuxue/app/testPaper/paperDetail", param);
};