import $http from '@/config/requestConfig'

const pay = {
   wxpay:"qiuxue/app/pay/payCallback/wx",//微信支付回调
   alipay:"qiuxue/app/pay/payCallback/zfb",//支付宝支付回调
   smallPay:"",//小程序支付回调
   unifiedPay:"qiuxue/app/pay/unifiedPay",//统一支付
   queryPayStatus:"qiuxue/app/pay/queryPayStatus",//支付状态
};
const address = {
   allList:'qiuxue/provinceCityArea/allList',//查询所用省市区列表
   areaList:'qiuxue/provinceCityArea/areaList',//根据市编码查询区县列表
   cityList:'qiuxue/provinceCityArea/cityList/',//根据省编码查询市列表
   provinceList:'qiuxue/provinceCityArea/provinceList',//查询省列表
};
export {
	pay,
	address
};

//userInfo
export const userInfo = (param) => {
    return $http.post('mobile/StudentUser/userInfo', param);
};
//login
export const login = (param) => {
    return $http.post("mobile/Login/login", param);
};
//微信getOpenId
export const getOpenId = (param) => {
    return $http.post("mobile/Login/oauth2", param);
};
//qqisbind
export const isQQBind = (param) => {
    return $http.post("mobile/Login/qqisbind", param);
};
//isbind
export const isbind = (param) => {
    return $http.post("mobile/Login/isbind", param);
};
//首页banner
export const getBannerList = () => {
    return $http.post("mobile/schoolIndex/ad", {pid: 1});
};
